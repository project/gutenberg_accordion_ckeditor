Provides a Gutenberg block that produces accordions compatible with
INTRODUCTION
------------

Provides a Gutenberg block that produces accordions compatible with CKEditor Accordion.
(For them to work, that module must be enabled).

REQUIREMENTS
------------

This module requires the following modules:

 * [CKEditor Accordion](https://www.drupal.org/project/ckeditor_accordion)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

However CKEditor Accordion may need configuration.

MODIFYING THE JAVASCRIPT
------------------------

# To build

npm run build && drush cr

# Editing and creating blocks:

- Only edit index.jsx, then compile via 'npm run build' or 'npm start'

- For npm start, make sure you have the inspector open
  and cache disabled when you refresh to get the latest changes.

# Copied from existing documentation:

MAINTAINERS
-----------

Current maintainers:
* Maxwell Keeble (maxwellkeeble) - https://www.drupal.org/u/maxwellkeeble
