import * as accordion from './accordion';

const { blocks, data } = wp;

const { registerBlockType } = blocks;
const { dispatch, select } = data;

const category = {
  slug: 'gutenberg-accordion-ckeditor',
  title: 'Accordion',
};

const currentCategories = select('core/blocks')
  .getCategories()
  .filter((item) => item.slug !== category.slug);

dispatch('core/blocks').setCategories([category, ...currentCategories]);

// ACCORDION
// https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/nested-blocks-inner-blocks/

registerBlockType(`${category.slug}/ckeditor-accordion-section`, {
  category: category.slug,
  parent: [`${category.slug}/ckeditor-accordion`],
  ...accordion.accordionSectionSettings,
});

registerBlockType(`${category.slug}/ckeditor-accordion`, {
  category: category.slug,
  ...accordion.accordionSettings,
});
