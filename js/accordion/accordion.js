const { element, editor, blockEditor } = wp;
const { Fragment } = element;
const {
    InnerBlocks,
} = blockEditor;
const __ = Drupal.t;

export default {
    title: __('Accordion'),
    description: __('Embed an accordion block (on drop down unfolds more text)'),
    edit: ({ className, attributes, setAttributes, isSelected }) => {
        return (
            <div className={className + " ckeditor-accordion"}>
                <InnerBlocks allowedBlocks={[`su-gutenberg-blocks/ckeditor-accordion-section`]} />
            </div>
        );
    },
    save: ({ attributes }) => {
        return (
            <Fragment>
                <dl class="ckeditor-accordion">
                    <InnerBlocks.Content />
                </dl>
            </Fragment>
        );
    },
};