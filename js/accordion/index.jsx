/**
 * Internal dependencies
 */
import icon from './icon';

import accordion from './accordion';
import accordionSection from './accordion-section';

export const accordionSectionSettings = {
    icon: icon,
    ...accordionSection
};

export const accordionSettings = {
    icon: icon,
    ...accordion
};
